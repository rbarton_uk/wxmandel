#include "canvas.h"

BEGIN_EVENT_TABLE(BasicGLPane, wxGLCanvas)
    EVT_MOTION(BasicGLPane::mouseMoved)
    EVT_LEFT_DOWN(BasicGLPane::mouseDown)
    EVT_LEFT_UP(BasicGLPane::mouseReleased)
    EVT_RIGHT_DOWN(BasicGLPane::rightClick)
    EVT_LEAVE_WINDOW(BasicGLPane::mouseLeftWindow)
    EVT_SIZE(BasicGLPane::resized)
    EVT_KEY_DOWN(BasicGLPane::keyPressed)
    EVT_KEY_UP(BasicGLPane::keyReleased)
    EVT_MOUSEWHEEL(BasicGLPane::mouseWheelMoved)
    EVT_PAINT(BasicGLPane::render)
    EVT_LEFT_DCLICK(BasicGLPane::mouseDouble)
    EVT_RIGHT_DCLICK(BasicGLPane::rightDouble)
END_EVENT_TABLE()

BasicGLPane::BasicGLPane(wxFrame *p, wxSize s)
    :wxGLCanvas(p, wxID_ANY, (const int*) NULL, wxDefaultPosition, s, wxFULL_REPAINT_ON_RESIZE), parent(p)
{
    context = new wxGLContext(this);
    SetCurrent(*context);
}

BasicGLPane::~BasicGLPane()
{
    gM.close();
    delete context;
}

void BasicGLPane::resized(wxSizeEvent& event)
{
//	wxGLCanvas::OnSize(evt);

    //Refresh();


/*    wxStatusBar* sb = parent->GetStatusBar();
    wxSize cs = GetSize();
    gSettings.winWidth = cs.GetWidth();
    gSettings.winHeight = cs.GetHeight();
    if (sb)
        sb->SetStatusText(wxString::Format(wxT("Resized to %d / %d"),gSettings.winWidth, gSettings.winHeight ),1); */
}

void BasicGLPane::render(wxPaintEvent& WXUNUSED(event))
{
    //if (!IsShown() || wxGetMouseState().LeftIsDown()) return;
    //SetCurrent(*m_context);
    wxPaintDC(this);
    gM.paint();
    glFlush();
    SwapBuffers();
}

void BasicGLPane::render()
{
    //if (!IsShown() || wxGetMouseState().LeftIsDown()) return;
    //SetCurrent(*m_context);
    wxClientDC(this);
    gM.paint();
    glFlush();
    SwapBuffers();
}

