#ifndef CANVAS_H_INCLUDED
#define CANVAS_H_INCLUDED
#include "globals.h"
#include <wx/glcanvas.h>

class BasicGLPane: public wxGLCanvas {

public:
    BasicGLPane(wxFrame *parent, wxSize s);
    virtual ~BasicGLPane();
	void resized(wxSizeEvent& evt);
	void render(wxPaintEvent& evt);
	void render();

	// events
	void mouseMoved(wxMouseEvent& event);
	void mouseDown(wxMouseEvent& event);
	void mouseWheelMoved(wxMouseEvent& event);
	void mouseReleased(wxMouseEvent& event);
	void rightClick(wxMouseEvent& event);
	void mouseLeftWindow(wxMouseEvent& event);
	void keyPressed(wxKeyEvent& event);
	void keyReleased(wxKeyEvent& event);
	void mouseDouble(wxMouseEvent& event);
	void rightDouble (wxMouseEvent& event);

private:
        wxFrame* parent;
        wxGLContext* context;
        wxPoint dragStart;

protected:
    DECLARE_EVENT_TABLE()
};

#endif // CANVAS_H_INCLUDED
