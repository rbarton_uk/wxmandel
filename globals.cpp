#include "globals.h"
#include "mandel.h"

Settings gSettings("settings.json");
Mset gM;

void gLog(wxString msg) {
    ((BasicApplication *)wxTheApp)->log(msg);
}

void gAlert(wxString msg)
{
    ((BasicApplication *)wxTheApp)->alert(msg);
}

void gStatus(wxString msg)
{
    ((BasicApplication *)wxTheApp)->status(msg);
}

void gShowParams()
{
    ((BasicApplication *)wxTheApp)->showParams();
}

void gSetZoom() {
    ((BasicApplication *)wxTheApp)->setZoom();
}


