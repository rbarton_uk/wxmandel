#ifndef GLOBALS_H
#define GLOBALS_H

#include <wx/wx.h>
#include "mset.h"
#include "settings.h"

extern Settings gSettings;
extern Mset gM;

void gLog (wxString msg);
void gAlert (wxString msg);
void gStatus (wxString msg);
void gShowParams ();
void gSetZoom();

#endif
