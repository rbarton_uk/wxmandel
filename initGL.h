#ifndef MSET_H_INCLUDED
#define MSET_H_INCLUDED
#include "mandel.h"

class Mset
{
public:
    Mset(BasicFrame *parent, Settings *settings): parent(parent), settings(settings) {};
    bool init();
    bool restart();
    bool resize();
    bool iterate();
    void paint();
    void close();
    GLuint LoadShaders(bool feedback, const char * vertex_file_path,const char * fragment_file_path);
    void GLAPIENTRY MessageCallback(GLenum source,
                                    GLenum type,
                                    GLuint id,
                                    GLenum severity,
                                    GLsizei length,
                                    const GLchar* message);

private:
    BasicFrame *parent;
    Settings *settings;
    void alert(wxString msg)
    {
        parent->alert(message);
    }
    void checkGLError(int l);
    void log(wxString msg)
    {
        parent->log(msg);
    }
    bool initGL();
    bool buildWinData(BasicGLPane *pane);
    static const GLfloat gScreenBD[] =
    {
        -1.0f, -1.0f,
            1.0f, -1.0f,
            -1.0f,  1.0f,
            -1.0f,  1.0f,
            1.0f, -1.0f,
            1.0f,  1.0f
        };

//Graphics program
    GLuint gTextureVA = 0;
    GLuint gScreenVA = 0;
    int gProgram = 0;
    GLuint gProgramID[] = {0,0};
    GLuint gScreenPID = 0;
    GLint gCentreLocation = -1;
    GLint gScaleLocation = -1;
    GLint gColMapLocation = -1;
    GLint gParamsLocation = -1;
    GLint gScrParamLocation = -1;
    GLint gTextureLocation = -1;
    GLint gInTexLocation = -1;

    GLuint gVBO = 0;
    GLuint gScreenBO = 0;
    GLuint gIBO = 0;
    GLuint gVAB = 0;
    GLuint gTFB = 0;
    GLuint gFramebuffer = 0;
    int gOutTexture = 0;
    GLuint gTexture[] = {0,0};
    GLenum gDrawBuffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    GLint gTexEmpty[] = {-1,-1,-1,-1};
    GLint gThreshold = 0;
    int gNoVertices = 0;
    int gNoCoords = 0;
    GLint *gVBD = nullptr;
    GLuint *gIBD = nullptr;
}

#endif

