#include "mandel.h"
#include <wx/filedlg.h>
#include <wx/filename.h>
#include <math.h>

IMPLEMENT_APP(BasicApplication)

bool BasicApplication::OnInit()
{
    frame = new BasicFrame(_T("wxMandel"), 50, 50, 450, 300);
    frame->Show(TRUE);
    SetTopWindow(frame);
    gM.init();
    gM.restart();
    gM.iterate();
    return TRUE;
}

void BasicApplication::log(wxString msg) {
    ((BasicFrame *)frame)->theText->SetInsertionPoint(0);
    ((BasicFrame *)frame)->theText->WriteText(msg);
    ((BasicFrame *)frame)->theText->WriteText(wxT("\n"));
}

void BasicApplication::alert(wxString msg)
{
    wxMessageDialog aboutDialog( frame, msg, _T("Alert"), wxOK | wxCANCEL );
    if (aboutDialog.ShowModal()==wxID_CANCEL)
        frame->Close(TRUE);
}

void BasicApplication::status(wxString msg) {
    ((BasicFrame *)frame)->SetStatusText(msg,0);
}

void BasicApplication::showParams() {
    ((BasicFrame *)frame)->SetStatusText(wxString::Format(wxT("Centre (%.3e,%.3e) Scale (%.3e,%.3e) Threshold: %i"), gSettings.centrer.h, gSettings.centrei.h, gSettings.scaler.h, gSettings.scalei.h, gSettings.thresh),1);
}


void BasicApplication::setZoom()
{
    ((BasicFrame *)frame)->zoomSlider->SetValue(-log10(gSettings.scaler.h)*100.0);
}

BasicFrame::BasicFrame( const wxChar *title, int xpos, int ypos, int width, int height)
    : wxFrame( (wxFrame*) NULL, -1, title, wxPoint(xpos, ypos), wxSize(width, height) )
{
    theText = (wxTextCtrl *) NULL;
    menuBar  = (wxMenuBar *) NULL;
    fileMenu = (wxMenu *) NULL;
    statusBar = (wxStatusBar *) NULL;
    logpane = (wxCollapsiblePane *) NULL;

    MyGLCanvas = new BasicGLPane(this, wxSize(gSettings.winWidth, gSettings.winHeight));

    wxBoxSizer *mainSizer = new wxBoxSizer(wxHORIZONTAL);
    logpane = new wxCollapsiblePane(this, -1, wxT("Log"), wxDefaultPosition, wxDefaultSize, wxCP_NO_TLW_RESIZE);
    wxWindow *cWin = logpane->GetPane();
    theText = new wxTextCtrl( cWin, -1,
                              wxString("Log:\n"
                                      ),
                              wxDefaultPosition,
                              wxDefaultSize,
                              wxTE_MULTILINE
                            );


    //logpane->Collapse(true);
    wxBoxSizer *sideSizer = new wxBoxSizer(wxVERTICAL);
    sideSizer->Add(logpane, 0, wxEXPAND, 0);
    zoomSlider = new wxSlider(this, -1, -log10(gSettings.scaler.h)*100.0, -50, 3000, wxDefaultPosition, wxDefaultSize, wxSL_VERTICAL);
    sideSizer->Add(zoomSlider, 1, wxEXPAND, 0);
    mainSizer->Add(sideSizer, 1, wxEXPAND, 0);
    mainSizer->Add(MyGLCanvas, 5, wxEXPAND,0);

    SetAutoLayout( TRUE );
    SetSizer( mainSizer );
    mainSizer->SetSizeHints( this );

    fileMenu = new wxMenu;
    fileMenu->Append(BASIC_OPEN,  _T("&Open Parameters"));
    fileMenu->Append(BASIC_SETTINGS,  _T("&Save Settings"));
    fileMenu->Append(BASIC_SCREEN, _T("Save Screen"));
    fileMenu->Append(BASIC_ABOUT, _T("&About"));
    fileMenu->AppendSeparator();
    fileMenu->Append(BASIC_EXIT,  _T("E&xit"));

    menuBar = new wxMenuBar;
    menuBar->Append(fileMenu, _T("&File"));
    SetMenuBar(menuBar);
    statusBar = CreateStatusBar(3);
    SetStatusText(wxGetCwd(),1);
    SetMinSize(ClientToWindowSize(wxSize(256, 256)));
}

BasicFrame::~BasicFrame()
{
}

BEGIN_EVENT_TABLE (BasicFrame, wxFrame)
    EVT_MENU ( BASIC_EXIT,  BasicFrame::OnExit)
    EVT_MENU ( BASIC_ABOUT, BasicFrame::OnAbout)
    EVT_MENU ( BASIC_OPEN,  BasicFrame::OnOpenFile)
    EVT_MENU ( BASIC_SETTINGS, BasicFrame::OnSaveSettings)
    EVT_MENU (BASIC_SCREEN, BasicFrame::OnSaveScreen)
    EVT_SIZE ( BasicFrame::OnSize)
    EVT_IDLE ( BasicFrame::OnIdle)
    EVT_SCROLL( BasicFrame:: OnZoom)
END_EVENT_TABLE()

void BasicFrame::OnOpenFile( wxCommandEvent & event )
{
    wxFileDialog* openFileDialog =
        new wxFileDialog( this, _("Open file"), wxGetCwd(), "", "JSON files|*.json",
                          wxFD_OPEN | wxFD_FILE_MUST_EXIST, wxDefaultPosition);

    if ( openFileDialog->ShowModal() == wxID_OK )
    {
        gSettings.load(openFileDialog->GetPath());
        SetStatusText(openFileDialog->GetFilename(), 0);
        SetStatusText(openFileDialog->GetDirectory(),1);
        gM.restart();
        gM.iterate();
        MyGLCanvas->render();
    }
}

void BasicFrame::OnSaveSettings( wxCommandEvent & event )
{
    wxFileDialog* saveFileDialog =
        new wxFileDialog( this, _("Save settings file"), wxGetCwd(), "", "json files (*.json)|*.json",
                          wxFD_SAVE | wxFD_OVERWRITE_PROMPT, wxDefaultPosition);

    if ( saveFileDialog->ShowModal() == wxID_OK )
    {
        gSettings.save(saveFileDialog->GetPath());
        SetStatusText(saveFileDialog->GetFilename(), 0);
        SetStatusText(saveFileDialog->GetDirectory(),1);
    }
}

void BasicFrame::OnSaveScreen( wxCommandEvent & event )
{
    wxFileDialog* saveFileDialog =
        new wxFileDialog( this, _("Save screen file"), wxGetCwd(), "", "png files (*.png)|*.png",
                          wxFD_SAVE | wxFD_OVERWRITE_PROMPT, wxDefaultPosition);

    if ( saveFileDialog->ShowModal() == wxID_OK )
    {
        gM.saveFrame(saveFileDialog->GetPath());
    }
}

void BasicFrame::OnAbout( wxCommandEvent & event )
{
    wxString t =_T("\nDB 2001");

    wxMessageDialog aboutDialog( this, t, _T("About Basic"), wxOK | wxCANCEL );
    aboutDialog.ShowModal();
}

void BasicFrame::OnExit( wxCommandEvent & event )
{
    Close(TRUE);
}

void BasicFrame::OnSize( wxSizeEvent & event )
{
    Layout();
    wxSize sz= MyGLCanvas->GetSize();
    int h = sz.GetHeight();
    int w = sz.GetWidth();
    if (gSettings.winWidth == w && gSettings.winHeight == h)
        return;
    gSettings.winWidth = w;
    gSettings.winHeight = h;
    gM.resize();
    gM.restart();
    gM.iterate();

    if (statusBar)
        statusBar->SetStatusText(wxString::Format(wxT("Canvas Size %d / %d"),w, h),2);
}

void BasicFrame::OnIdle( wxIdleEvent & event )
{
    if (gM.iterating())
    {
        gM.iterate();
        MyGLCanvas->render();
    }
    if (gM.iterating())
    {
        event.RequestMore();
    }
}

void BasicFrame::OnZoom(wxScrollEvent & event) {
    float pos = event.GetPosition();
    gSettings.scaler = Quad(pow(10,-pos/100.0));
    gSettings.scalei = gSettings.scaler.mul(Quad((double)gSettings.winHeight/gSettings.winWidth));
    gSetZoom();
    gM.restart();
    gM.iterate();
    MyGLCanvas->render();
}
