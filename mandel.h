#ifndef BASIC_H
#define BASIC_H
#include <GL/glew.h>
#include <wx/wx.h>
#include "canvas.h"
#include "globals.h"
#include <wx/collpane.h>

class BasicApplication : public wxApp
{
public:
	virtual bool OnInit();
	void log(wxString msg);
	void alert(wxString msg);
	void status(wxString msg);
	void showParams();
    void setZoom();

private:
    wxFrame *frame;
};

class BasicFrame : public wxFrame
{
public:
	BasicFrame( const wxChar *title, int xpos, int ypos, int width, int height );
	~BasicFrame();

    BasicGLPane *MyGLCanvas;
	wxTextCtrl *theText;
	wxMenuBar  *menuBar;
	wxMenu     *fileMenu;
	wxStatusBar *statusBar;
	wxCollapsiblePane *logpane;
	wxSlider    *zoomSlider;

	void OnOpenFile (wxCommandEvent & event);
	void OnSaveSettings (wxCommandEvent & event);
	void OnSaveScreen (wxCommandEvent & event);
	void OnAbout    (wxCommandEvent & event);
	void OnExit     (wxCommandEvent & event);
	void OnSize     (wxSizeEvent & event);
	void OnIdle     (wxIdleEvent& event);
	void OnZoom     (wxScrollEvent & event);

	DECLARE_EVENT_TABLE()
};

enum {
	BASIC_EXIT  =   1,
	BASIC_OPEN  = 100,
	BASIC_SETTINGS  = 150,
	BASIC_SCREEN,
	BASIC_ABOUT = 200
};

#endif
