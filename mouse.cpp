#include "canvas.h"


void BasicGLPane::mouseMoved(wxMouseEvent& event)
{
    if (event.Dragging())
    {
        int x=event.GetX()-dragStart.x;
        int y=event.GetY()-dragStart.y;
        if (wxGetKeyState(WXK_CONTROL))
        {
            gSettings.hueScale -= gSettings.hueStep * y / 100.0;
            gSettings.baseHue += gSettings.hueFraction + x /100.0;
            render();
        }
        else
        {
            gSettings.centrer = gSettings.centrer.add(gSettings.scaler.mul(Quad((double)-x / gSettings.winWidth)));
            gSettings.centrei = gSettings.centrei.add(gSettings.scalei.mul(Quad((double)y / gSettings.winHeight)));
            gM.restart();
            gM.iterate();
            render();
        }
        dragStart = event.GetPosition();
    }
//        sb->SetStatusText(wxString::Format(wxT("%d% / %d%"),event.GetX()*100/cs.GetWidth(), event.GetY() * 100 / cs.GetHeight() ),1);
}
void BasicGLPane::mouseWheelMoved(wxMouseEvent& event)
{
    if (wxGetKeyState(WXK_CONTROL))
    {
        if (event.GetWheelRotation()>1)
        {
            if (gSettings.thresh == gSettings.maxThresh)
                return;
            gSettings.thresh *= 1.0 + gSettings.threshFraction;
            if (gSettings.thresh > gSettings.maxThresh)
                gSettings.thresh = gSettings.maxThresh;
            gM.restart();
            gM.iterate();
            render();
        }
        else
        {
            if (gSettings.thresh == gSettings.minThresh)
                return;
            gSettings.thresh /= 1.0 + gSettings.threshFraction;
            if (gSettings.thresh < gSettings.minThresh)
                gSettings.thresh = gSettings.minThresh;
            gM.restart();
            gM.iterate();
            render();
        }
    }
    else
    {
        int dir = (event.GetWheelRotation()>1)?1.0:-1.0;
        gSettings.centrer = gSettings.centrer.add(gSettings.scaler.mul(Quad(2.0 * event.GetX() / gSettings.winWidth - 1.0)));
        gSettings.centrei = gSettings.centrei.add(gSettings.scalei.mul(Quad(1.0 - 2.0 * event.GetY() / gSettings.winHeight)));
        gSettings.scaler = gSettings.scaler.mul(Quad(1.0 - dir*gSettings.zoomFraction));
        gSettings.scalei = gSettings.scaler.mul(Quad((double)gSettings.winHeight/gSettings.winWidth));
        WarpPointer(gSettings.winWidth/2,gSettings.winHeight/2);
        gSetZoom();
        gM.restart();
        gM.iterate();
        render();
    }
}

void BasicGLPane::mouseDown(wxMouseEvent& event)
{
    dragStart=event.GetPosition();
}
void BasicGLPane::mouseReleased(wxMouseEvent& event) {}
void BasicGLPane::mouseLeftWindow(wxMouseEvent& event) {}
void BasicGLPane::mouseDouble(wxMouseEvent& event)
{
    gSettings.centrer = gSettings.centrer.add(gSettings.scaler.mul(Quad(2.0 * event.GetX() / gSettings.winWidth - 1.0)));
    gSettings.centrei = gSettings.centrei.add(gSettings.scalei.mul(Quad(1.0 - 2.0 * event.GetY() / gSettings.winHeight)));
    gSettings.scaler = gSettings.scaler.mul(Quad(1.0 - gSettings.zoomFraction));
    gSettings.scalei = gSettings.scaler.mul(Quad((double)gSettings.winHeight/gSettings.winWidth));
    WarpPointer(gSettings.winWidth/2,gSettings.winHeight/2);
    gSetZoom();
    gM.restart();
    gM.iterate();
    render();
}
void BasicGLPane::rightClick(wxMouseEvent& event) {}
void BasicGLPane::rightDouble(wxMouseEvent& event)
{
    gSettings.centrer = gSettings.centrer.add(gSettings.scaler.mul(Quad(2.0 * event.GetX() / gSettings.winWidth - 1.0)));
    gSettings.centrei = gSettings.centrei.add(gSettings.scalei.mul(Quad(1.0 - 2.0 * event.GetY() / gSettings.winHeight)));
    gSettings.scaler = gSettings.scaler.mul(Quad(1.0 + gSettings.zoomFraction));
    gSettings.scalei = gSettings.scaler.mul(Quad((double)gSettings.winHeight/gSettings.winWidth));
    WarpPointer(gSettings.winWidth/2,gSettings.winHeight/2);
    gSetZoom();
    gM.restart();
    gM.iterate();
    render();
}
