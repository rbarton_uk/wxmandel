#include "mset.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

static const GLfloat gScreenBD[] =
{
    -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f,  1.0f,
        -1.0f,  1.0f,
        1.0f, -1.0f,
        1.0f,  1.0f
    };

void alert(wxString message, BasicGLPane *pane);
void checkGLError(int l, BasicGLPane *pane);



//Graphics program
GLuint gTextureVA = 0;
GLuint gScreenVA = 0;
int gProgram = 0;
GLuint gProgramID[] = {0,0};
GLuint gScreenPID = 0;
GLint gCentreLocation = -1;
GLint gScaleLocation = -1;
GLint gColMapLocation = -1;
GLint gParamsLocation = -1;
GLint gScrParamLocation = -1;
GLint gTextureLocation = -1;
GLint gInTexLocation = -1;

GLuint gVBO = 0;
GLuint gScreenBO = 0;
GLuint gIBO = 0;
GLuint gVAB = 0;
GLuint gTFB = 0;
GLuint gFramebuffer = 0;
int gOutTexture = 0;
GLuint gTexture[] = {0,0};
GLenum gDrawBuffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
GLint gTexEmpty[] = {-1,-1,-1,-1};
GLint gThreshold = 0;
int gNoVertices = 0;
int gNoCoords = 0;
GLint *gVBD = nullptr;
GLuint *gIBD = nullptr;

void paint(BasicGLPane *pane)
{
    wxSize window = pane->GetSize();
    int res = pane->settings->minRes;
    int pointWidth = pane->settings->winWidth / res;
    int pointHeight = pane->settings->winHeight / res;
    int noIndices = pointWidth * pointHeight;
    if (gIBD != nullptr)
    {
        delete[] gIBD;
    }
    gIBD = new GLuint[noIndices];
    int i = 0;
    int ystep = pane->settings->winWidth;
    for (int v = 0; v < pointHeight; v++)
    {
        int y = (v+1)/2*((v%2)*2-1)+(pointHeight-1)/2;
        int basey = y * res * ystep;
        for (int w = 0; w < pointWidth; w++)
        {
            int x = (w+1)/2*((w%2)*2-1)+(pointWidth-1)/2;
            int base = x * res + basey;
            if (i < noIndices)
            {
                gIBD[i++] = base;
            }
            else
            {
                alert(wxString::Format(wxT("Overflow gIBD. Window (%i,%i) loop (%i,%i) res %i\n"), pane->settings->winWidth, pane->settings->winHeight, x, y, res), pane);
            }
        }
    }
    glBindVertexArray(gTextureVA);
    checkGLError(__LINE__, pane);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
    checkGLError(__LINE__, pane);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * noIndices, gIBD, GL_STATIC_DRAW);
    checkGLError(__LINE__, pane);

    int indstep = noIndices;
    long long total_iter = (long long) noIndices * gThreshold * (1 + (pane->settings->highCapFactor - 1)*gProgram);
    while (total_iter > pane->settings->frameCap && indstep > 1 && (res != 1))
    {
        total_iter /= 2;
        indstep /= 2;
    }
    i = 0;
    if (res != 1)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
        glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
        glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gTexture[1-gOutTexture], 0);
        glReadBuffer(GL_COLOR_ATTACHMENT1);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        glBlitFramebuffer(0, 0, pane->settings->winWidth, pane->settings->winHeight, 0, 0, pane->settings->winWidth, pane->settings->winHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
        gOutTexture = 1 - gOutTexture;

        alert(wxString::Format(wxT("Resolution %i %i%"),res, (int)((100.0*i/noIndices))), pane);
        glBindVertexArray(gTextureVA);
        checkGLError(__LINE__, pane);

        glUseProgram(gProgramID[gProgram]);
        checkGLError(__LINE__, pane);

        // 1st attribute buffer : vertices
        glEnableVertexAttribArray(0);
        checkGLError(__LINE__, pane);

        glBindBuffer(GL_ARRAY_BUFFER, gVBO);
        checkGLError(__LINE__, pane);

        glVertexAttribIPointer(
            0,		  // attribute 0. No particular reason for 0, but must match the layout in the shader.
            2,		  // size
            GL_INT, // type
//						GL_FALSE, // normalized?
            0,		  // stride
            (void *)0 // array buffer offset
        );
        checkGLError(__LINE__, pane);

        glActiveTexture(GL_TEXTURE0);
        checkGLError(__LINE__, pane);

        glBindTexture(GL_TEXTURE_2D, gTexture[1-gOutTexture]);
        checkGLError(__LINE__, pane);

        glUniform1i(gInTexLocation, 0);
        checkGLError(__LINE__, pane);

        glUniform4d(gCentreLocation, pane->settings->centrer.h, pane->settings->centrer.l, pane->settings->centrei.h, pane->settings->centrei.l);
        checkGLError(__LINE__, pane);

        glUniform4d(gScaleLocation, pane->settings->scaler.h, pane->settings->scaler.l, pane->settings->scalei.h, pane->settings->scalei.l);
        checkGLError(__LINE__, pane);

        glUniform4i(gParamsLocation, gThreshold,pane->settings->winWidth,pane->settings->winHeight,res);
        checkGLError(__LINE__, pane);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
        checkGLError(__LINE__, pane);

        int noElements = indstep;
        if (i+noElements >= noIndices) noElements = noIndices - i;

        glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
        checkGLError(__LINE__, pane);

        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gTexture[gOutTexture], 0);
        checkGLError(__LINE__, pane);

        glDrawBuffers(2, gDrawBuffers);
        checkGLError(__LINE__, pane);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            alert(wxString::Format("Framebuffer not complete at line %i\n",__LINE__), pane);

        glViewport(0,0,pane->settings->winWidth,pane->settings->winHeight);

        glDrawElements(GL_POINTS, noElements, GL_UNSIGNED_INT, (void *)(i * sizeof(GLuint)));
        checkGLError(__LINE__, pane);

        glDisableVertexAttribArray(0);
        checkGLError(__LINE__, pane);
    }

    glBindVertexArray(gScreenVA);
    checkGLError(__LINE__, pane);

    glUseProgram(gScreenPID);
    checkGLError(__LINE__, pane);

    glEnableVertexAttribArray(0);
    checkGLError(__LINE__, pane);

    glBindBuffer(GL_ARRAY_BUFFER, gScreenBO);
    checkGLError(__LINE__, pane);

    glVertexAttribPointer(
        0,		  // attribute 0. No particular reason for 0, but must match the layout in the shader.
        2,		  // size
        GL_FLOAT, // type
        GL_FALSE, // normalized?
        0,		  // stride
        (void *)0 // array buffer offset
    );
    checkGLError(__LINE__, pane);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    checkGLError(__LINE__, pane);

    glViewport(0,0,pane->settings->winWidth, pane->settings->winHeight);
    checkGLError(__LINE__, pane);

    glActiveTexture(GL_TEXTURE0);
    checkGLError(__LINE__, pane);

    glBindTexture(GL_TEXTURE_2D, gTexture[gOutTexture]);
    checkGLError(__LINE__, pane);

    glUniform1i(gTextureLocation, 0);
    checkGLError(__LINE__, pane);

    glUniform4i(gScrParamLocation, gThreshold, res, pane->settings->winWidth, pane->settings->winHeight);
    checkGLError(__LINE__, pane);

    glUniform2f(gColMapLocation, pane->settings->baseHue, pane->settings->hueScale);
    checkGLError(__LINE__, pane);

    // Draw the triangle !
    glDrawArrays(GL_TRIANGLES, 0, 2*3); // 12*3 indices starting at 0 -> 12 triangles -> 6 squares
    checkGLError(__LINE__, pane);

    glDisableVertexAttribArray(0);
    checkGLError(__LINE__, pane);
}

bool initGL()
{
    //Success flag
    bool success = true;

    glGenVertexArrays(1, &gTextureVA);
    checkGLError(__LINE__, pane);

    glBindVertexArray(gTextureVA);
    checkGLError(__LINE__, pane);

    // Generate 1 buffer, put the resulting identifier in vertexbuffer
    glGenBuffers(1, &gVBO);
    checkGLError(__LINE__, pane);

    glGenBuffers(1, &gVAB);
    checkGLError(__LINE__, pane);

    glGenBuffers(1, &gTFB);
    checkGLError(__LINE__, pane);

    glGenBuffers(1, &gIBO);
    checkGLError(__LINE__, pane);

    glGenTextures(2, gTexture);
    checkGLError(__LINE__, pane);

    glBindTexture(GL_TEXTURE_2D, gTexture[0]);
    checkGLError(__LINE__, pane);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, pane->settings->winWidth, pane->settings->winHeight, 0,GL_RED_INTEGER, GL_UNSIGNED_BYTE, 0);
    checkGLError(__LINE__, pane);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    checkGLError(__LINE__, pane);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    checkGLError(__LINE__, pane);

    glBindTexture(GL_TEXTURE_2D, gTexture[1]);
    checkGLError(__LINE__, pane);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, pane->settings->winWidth, pane->settings->winHeight, 0,GL_RED_INTEGER, GL_UNSIGNED_BYTE, 0);
    checkGLError(__LINE__, pane);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    checkGLError(__LINE__, pane);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    checkGLError(__LINE__, pane);

    glGenFramebuffers(1, &gFramebuffer);
    checkGLError(__LINE__, pane);

    glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
    checkGLError(__LINE__, pane);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gTexture[0], 0);
    checkGLError(__LINE__, pane);

    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    glClearBufferiv(GL_COLOR, 0, gTexEmpty);
    checkGLError(__LINE__, pane);

    glDrawBuffers(2, gDrawBuffers);
    checkGLError(__LINE__, pane);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) return false;

    glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
    glViewport(0,0,pane->settings->winWidth, pane->settings->winHeight);

    glGenVertexArrays(1, &gScreenVA);
    checkGLError(__LINE__, pane);

    glBindVertexArray(gScreenVA);
    checkGLError(__LINE__, pane);

    glGenBuffers(1, &gScreenBO);
    checkGLError(__LINE__, pane);

    glBindBuffer(GL_ARRAY_BUFFER, gScreenBO);
    checkGLError(__LINE__, pane);

    glBufferData(GL_ARRAY_BUFFER, sizeof(gScreenBD), gScreenBD, GL_STATIC_DRAW);
    checkGLError(__LINE__, pane);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0,0,pane->settings->winWidth, pane->settings->winHeight);

    glEnableVertexAttribArray(0);
    checkGLError(__LINE__, pane);

    glBindBuffer(GL_ARRAY_BUFFER, gScreenBO);
    checkGLError(__LINE__, pane);

    glVertexAttribPointer(
        0,		  // attribute 0. No particular reason for 0, but must match the layout in the shader.
        2,		  // size
        GL_FLOAT, // type
        GL_FALSE, // normalized?
        0,		  // stride
        (void *)0 // array buffer offset
    );
    checkGLError(__LINE__, pane);

    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

    gScreenPID = LoadShaders(false, "passthroughshader.txt", "textureshader.txt", pane);

    gTextureLocation = glGetUniformLocation(gScreenPID, "tex");
    checkGLError(__LINE__, pane);
    if (gTextureLocation == -1)
    {
        alert(wxT("Failed to get texture location\n"), pane);
        success = false;
        return success;
    }
    checkGLError(__LINE__, pane);

    gScrParamLocation = glGetUniformLocation(gScreenPID, "params");
    checkGLError(__LINE__, pane);
    if (gScrParamLocation == -1)
    {
        alert(wxT("Failed to get params locations\n"), pane);
        success = false;
        return success;
    }

    gColMapLocation = glGetUniformLocation(gScreenPID, "colmap");
    checkGLError(__LINE__, pane);
    if (gColMapLocation == -1)
    {
        alert(wxT("Failed to get colour map locations\n"), pane);
        success = false;
        return success;
    }

    gProgramID[0] = LoadShaders(true, pane->settings->lowShader.c_str(), "tfshader.txt",pane);
    checkGLError(__LINE__, pane);

    gProgramID[1] = LoadShaders(true, pane->settings->highShader.c_str(), "tfshader.txt", pane);
    checkGLError(__LINE__, pane);

    gCentreLocation = glGetUniformLocation(gProgramID[0], "centre");
    checkGLError(__LINE__, pane);
    if (gCentreLocation == -1)
    {
        alert(wxT("Failed to get centre location\n"), pane);
        success = false;
        return success;
    }

    gScaleLocation = glGetUniformLocation(gProgramID[0], "scale");
    checkGLError(__LINE__, pane);
    if (gScaleLocation == -1)
    {
        alert(wxT("Failed to get scale locations\n"),pane);
        success = false;
        return success;
    }

    gParamsLocation = glGetUniformLocation(gProgramID[0], "params");
    checkGLError(__LINE__, pane);
    if (gParamsLocation == -1)
    {
        alert(wxT("Failed to get params locations\n"), pane);
        success = false;
        return success;
    }

    gInTexLocation = glGetUniformLocation(gProgramID[0], "inTex");
    checkGLError(__LINE__, pane);
    if (gInTexLocation == -1)
    {
        alert(wxT("Failed to get inTex location\n"), pane);
        success = false;
        return success;
    }
    checkGLError(__LINE__, pane);

    gThreshold = pane->settings->minThresh;


    // During init, enable debug output
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, 0);

    return buildWinData(pane);
}

bool buildWinData(BasicGLPane *pane)
{
    bool success = true;

    gNoVertices = pane->settings->winWidth * pane->settings->winHeight;
    gNoCoords = gNoVertices * 2;

    if (gVBD != nullptr)
    {
        alert(wxT("About to delete gVBD\n"),pane);
        delete[] gVBD;
    }
    alert(wxT("About to create gVBD\n"), pane);
    gVBD = new GLint[gNoCoords];
    int i = 0;
    for (int y = 0; y < pane->settings->winHeight; y++)
    {
        for (int x = 0; x < pane->settings->winWidth; x++)
        {
            gVBD[i++] = x;
            gVBD[i++] = y;
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, gVBO);
    checkGLError(__LINE__, pane);

    glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * gNoCoords, gVBD, GL_STATIC_DRAW);
    checkGLError(__LINE__, pane);

    glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
    glDrawBuffers(2, gDrawBuffers);
    glClearBufferiv(GL_COLOR, 1, gTexEmpty);
    checkGLError(__LINE__, pane);

    return success;
}


