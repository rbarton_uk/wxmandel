#include "mset.h"
#include "globals.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

bool Mset::init()
{
    bool success = true;
    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();
    if (glewError != GLEW_OK)
    {
        gAlert(wxString::Format(wxT("Error initializing GLEW! %s\n"), glewGetErrorString(glewError)));
    }
    if (!initGL())
    {
        gAlert(wxT("Unable to initialize OpenGL!\n"));
        success = false;
    }
    gInitialised = success;
    stbi_flip_vertically_on_write(1);
    return success;
}

/*void GLAPIENTRY Mset::MessageCallback(GLenum source,
                                GLenum type,
                                GLuint id,
                                GLenum severity,
                                GLsizei length,
                                const GLchar* message)
{
    if (type != 0x8251)
        alert(wxString::Format(wxT("GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n"),
                               (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
                               type, severity, message));
} */

void Mset::close()
{
    if (!gInitialised) return;
    //Deallocate program
    glDeleteProgram(gProgramID[0]);
    checkGLError(__LINE__);
    glDeleteProgram(gProgramID[1]);
    checkGLError(__LINE__);
}

void Mset::checkGLError(int l)
{
    GLenum err;
    //	printf("Checking line %i\n", l);
    while ((err = glGetError()))
    {
        gAlert(wxString::Format(wxT("GL error at line %i : %i\n"), l, err));
    }
}

bool Mset::precision()
{
    gProgram = 1-gProgram;

    gCentreLocation = glGetUniformLocation(gProgramID[gProgram], "centre");
    checkGLError(__LINE__);
    if (gCentreLocation == -1)
    {
        gAlert(wxT("Failed to get centre location"));
        return false;
    }

    gScaleLocation = glGetUniformLocation(gProgramID[gProgram], "scale");
    checkGLError(__LINE__);
    if (gScaleLocation == -1)
    {
        gAlert(wxT("Failed to get scale locations"));
        return false;
    }

    gParamsLocation = glGetUniformLocation(gProgramID[gProgram], "params");
    checkGLError(__LINE__);
    if (gParamsLocation == -1)
    {
        gAlert(wxT("Failed to get params locations"));
        return false;
    }

    gInTexLocation = glGetUniformLocation(gProgramID[gProgram], "inTex");
    checkGLError(__LINE__);
    if (gInTexLocation == -1)
    {
        gAlert(wxT("Failed to get inTex location"));
        return false;
    }
    checkGLError(__LINE__);

    if (gProgram)
        gAlert(wxT("Switched to Quad(128bit) precision\n"));
    else
        gAlert(wxT("Switched to Double(64bit) precision\n"));

    return true;
}

void Mset::saveFrame(const char *filename) {
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    int x = viewport[0];
    int y = viewport[1];
    int width = viewport[2];
    int height = viewport[3];

    char *data = (char*) malloc((size_t) (width * height * 3)); // 3 components (R, G, B)

    if (!data)
        gAlert(wxT("No screen data to save"));

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glReadPixels(x, y, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);

    int saved = stbi_write_png(filename, width, height, 3, data, 0);

    free(data);

}
